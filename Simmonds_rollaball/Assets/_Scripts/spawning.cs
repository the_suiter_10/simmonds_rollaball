﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawning : MonoBehaviour
{
    private float nextSpawnTime;

    [SerializeField]
    private GameObject pickups;
    [SerializeField]
    private float spawnDelay = 3;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (ShouldSpawn())
        {
            Spawn();
        }
    }
    private bool ShouldSpawn()
    {
        return Time.time >= nextSpawnTime;
    }
    private void Spawn()
    {
        nextSpawnTime = Time.time + spawnDelay;
        //Instantiate(pickups, transform.position, transform.rotation);
    }
}
 
