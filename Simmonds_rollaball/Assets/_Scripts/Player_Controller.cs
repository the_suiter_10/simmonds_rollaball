﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{
    private const string V = "Purdue: 71";
    public float speed;
    public float jumpPower;
    public Text countText;
    public Text winText;
    public Text loseText;
    public bool won;
    public bool loss;
    bool onGround;
    public GameObject pickupPrefab;

    private Rigidbody rb;
    private int count;
    int totalPickups;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 56;
        SetCountText();
        winText.text = "";
        won = false;
        loss = false;
        GameObject[] allPickups = GameObject.FindGameObjectsWithTag("Pickup");
        totalPickups = allPickups.Length;
    }

    // Update is called once per frame
    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .55f);
        if (Input.GetKeyDown(KeyCode.Space) && onGround)
            //adding keys to be able to shoot the player at the ball
        {
            Jump();
        }
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpPower);
        //"Shooting" code
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");


        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 2;
            //changing the amount of points that are scored to mimic a basketball game
            SetCountText();
            Invoke("SpawnPickup", 3f);
            //special code to make the pickups spawn in the hoop at the correct times
        }
    }
    void SetCountText ()
    {
        countText.text = "IU: " + (count.ToString()) + "  " + V;
        if (count >= 72)
        {
            winText.text = "IU Wins!";
            won = true;
            //text changed to show a basketball like score
        }
    }
    void SpawnPickup ()
    {
        Vector3 randomPosition = new Vector3( .0f, 4.31f, 7.66f);
        Instantiate(pickupPrefab, randomPosition, pickupPrefab.transform.rotation);
        //code to insure that the ball respawns in the hoop every time
    }
}
